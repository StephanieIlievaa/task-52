import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  const button = document.querySelector(".button");
  button.addEventListener("click", () => {
    alert("💣");

  });
  
  document.body.addEventListener("click", createArticle);

  function createArticle() {
    for (let i = 0; i <= 4; i++) {
      let body = document.querySelector('.main');
      let article = document.createElement('article');
      article.textContent = 'Article created dinamically with JavaScript! '
      article.classList.add('message');
      body.appendChild(article);
    }
  };
});
